package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        double rows = (Math.sqrt(1 + 8 * inputNumbers.size()) - 1) / 2;
        if (rows % 1 != 0) {                                                 //check if we are able to build a pyramid
            throw new CannotBuildPyramidException();
        }

        int pyramidRows = (int) rows;


        try {
            inputNumbers.sort(Comparator.naturalOrder());
        } catch (NullPointerException | OutOfMemoryError e) {
            throw new CannotBuildPyramidException();
        }

        int[][] pyramid = new int[pyramidRows][pyramidRows * 2 - 1];      //filling array with zeroes
        for (int i = 0; i < pyramidRows; i++) {
            for (int j = 0; j < pyramidRows; j++) {
                pyramid[i][j] = 0;
            }
        }

        int bricks = inputNumbers.size() - 1;                               //building the pyramid
        for (int i = 0; i < pyramidRows; i++) {
            for (int j = pyramidRows - i - 1; j < pyramidRows + i; j = j + 2) {
                pyramid[i][j] = inputNumbers.get(inputNumbers.size() - 1 - bricks);
                bricks--;
            }
        }


        return pyramid;
    }


}
