package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null){
            throw new IllegalArgumentException();
        }

        if (x.size() > y.size()) {     //compare the size of lists, we can't build List x from List y if List y is shorter.
            return false;
        }

        int count = 0;              //counter for equal elements
        int lastIndex = 0;          //counter for last used index in List y
        for (int i = 0; i < x.size(); i++) {
            for (int j = lastIndex; j < y.size(); j++) {

                if (x.get(i).equals(y.get(j))) {
                    lastIndex = j;
                    count++;
                    break;
                }

            }

        }

        return count == x.size();   //if number of equal elements equals List x size method returns TRUE
    }
}
