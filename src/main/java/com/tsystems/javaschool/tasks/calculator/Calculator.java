package com.tsystems.javaschool.tasks.calculator;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     * parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    private TokenStack operatorStack;       //stack of operators + - * / and parentheses ( )
    private TokenStack valueStack;          //stack of double values
    private boolean error;                  //if error is True evaluate() returns null

    public Calculator() {
        operatorStack = new TokenStack();
        valueStack = new TokenStack();
        error = false;
    }

    public String evaluate(String statement) {
        if (statement == null || statement.isEmpty()) {
            return null;
        }
        String[] parts = statement.split("(?=[^\\d.])|(?<![\\d.])");            //regular expression to split the statement
        Token[] tokens = new Token[parts.length];
        for (int n = 0; n < parts.length; n++) {
            tokens[n] = new Token(parts[n]);
        }

        // Main loop - process all statement tokens
        for (int n = 0; n < tokens.length; n++) {
            Token nextToken = tokens[n];
            if (nextToken.getType() == Token.NUMBER) {
                valueStack.push(nextToken);
            } else if (nextToken.getType() == Token.OPERATOR) {
                if (operatorStack.isEmpty() || nextToken.getPrecedence() > operatorStack.top().getPrecedence()) {
                    operatorStack.push(nextToken);
                } else {
                    while (!operatorStack.isEmpty() && nextToken.getPrecedence() <= operatorStack.top().getPrecedence()) {
                        Token toProcess = operatorStack.top();
                        operatorStack.pop();
                        processOperator(toProcess);
                    }
                    operatorStack.push(nextToken);
                }
            } else if (nextToken.getType() == Token.LEFT_PARENTHESIS) {
                operatorStack.push(nextToken);
            } else if (nextToken.getType() == Token.RIGHT_PARENTHESIS) {
                while (!operatorStack.isEmpty() && operatorStack.top().getType() == Token.OPERATOR) {
                    Token toProcess = operatorStack.top();
                    operatorStack.pop();
                    processOperator(toProcess);
                }
                if (!operatorStack.isEmpty() && operatorStack.top().getType() == Token.LEFT_PARENTHESIS) {
                    operatorStack.pop();
                } else {
                    error = true;
                }
            }

        }
        // Empty out the operator stack at the end of the statement
        while (!operatorStack.isEmpty() && operatorStack.top().getType() == Token.OPERATOR) {
            Token toProcess = operatorStack.top();
            operatorStack.pop();
            processOperator(toProcess);
        }
        // Print the result if no error has been seen.
        if (error == false) {
            Token result = valueStack.top();
            valueStack.pop();
            if (!operatorStack.isEmpty() || !valueStack.isEmpty()) {
                return null;
            } else {

                NumberFormat numberFormatter = NumberFormat.getNumberInstance(Locale.US);
                numberFormatter.setMaximumFractionDigits(4);
                numberFormatter.setMinimumFractionDigits(0);

                System.out.println(numberFormatter.format(result.getValue()));
                return numberFormatter.format(result.getValue());
            }
        } else return null;
    }

    private void processOperator(Token t) {
        Token A = null, B = null;
        if (valueStack.isEmpty()) {
            error = true;
            return;
        } else {
            B = valueStack.top();
            valueStack.pop();
        }
        if (valueStack.isEmpty()) {
            error = true;
            return;
        } else {
            A = valueStack.top();
            valueStack.pop();
        }
        Token R = t.operate(A.getValue(), B.getValue());
        valueStack.push(R);
    }

    class TokenStack {
        /**
         * Member variables
         **/
        private ArrayList<Token> tokens;

        /**
         * Constructors
         **/
        public TokenStack() {
            tokens = new ArrayList<Token>();
        }

        /**
         * Accessor methods
         **/
        public boolean isEmpty() {
            return tokens.size() == 0;
        }

        public Token top() {
            return tokens.get(tokens.size() - 1);
        }

        /**
         * Mutator methods
         **/
        public void push(Token t) {
            tokens.add(t);
        }

        public void pop() {
            tokens.remove(tokens.size() - 1);
        }
    }

    //statement is split into tokens
    //operators and parentheses added to operatorStack
    //values added to valueStack
    class Token {
        public static final int UNKNOWN = -1;
        public static final int NUMBER = 0;
        public static final int OPERATOR = 1;
        public static final int LEFT_PARENTHESIS = 2;
        public static final int RIGHT_PARENTHESIS = 3;

        private int type;
        private double value;
        private char operator;
        private int precedence;

        public Token() {
            type = UNKNOWN;
        }

        public Token(String contents) {
            switch (contents) {
                case "+":
                    type = OPERATOR;
                    operator = contents.charAt(0);
                    precedence = 1;
                    break;
                case "-":
                    type = OPERATOR;
                    operator = contents.charAt(0);
                    precedence = 1;
                    break;
                case "*":
                    type = OPERATOR;
                    operator = contents.charAt(0);
                    precedence = 2;
                    break;
                case "/":
                    type = OPERATOR;
                    operator = contents.charAt(0);
                    precedence = 2;
                    break;
                case "(":
                    type = LEFT_PARENTHESIS;
                    break;
                case ")":
                    type = RIGHT_PARENTHESIS;
                    break;
                default:
                    type = NUMBER;
                    try {
                        value = Double.parseDouble(contents);
                    } catch (Exception ex) {
                        type = UNKNOWN;
                    }
            }
        }

        public Token(double x) {
            type = NUMBER;
            value = x;
        }

        int getType() {
            return type;
        }

        double getValue() {
            return value;
        }

        int getPrecedence() {
            return precedence;
        }


        Token operate(double a, double b) {
            double result = 0;
            switch (operator) {
                case '+':
                    result = a + b;
                    break;
                case '-':
                    result = a - b;
                    break;
                case '*':
                    result = a * b;
                    break;
                case '/':
                    if (b == 0) {
                        error = true;               //error is true if we divide by zero
                        break;
                    }
                    result = a / b;
                    break;
            }
            return new Token(result);
        }
    }
}
